import "./App.css";
import { Sudoku } from "./component/Sudoku";
import sudoku from "./store/sudoku";

function App() {
  return (
    <>
      <h1>SuDoKu</h1>
      <div className="card">
        <Sudoku sudoku={sudoku} />
      </div>
    </>
  );
}

export default App;
