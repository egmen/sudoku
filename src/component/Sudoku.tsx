import { observer } from "mobx-react";
import { Button, ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import "./sudoku.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose, faCopy, faPaste } from "@fortawesome/free-solid-svg-icons";
import { SudokuStore } from "../store/sudoku";
import { SudokuBoardComponent } from "./SudokuBoard";

function nextStepButtonName(step: SudokuStore["nextStep"]) {
  if (step === "calc") {
    return "Посчитать";
  }
  if (step === "clone") {
    return "Варианты";
  }
  if (step === "delete") {
    return "Удалить";
  }
  if (step === "next") {
    return "Следующая";
  }
  return `Неизвестных ход: ${step}`;
}

export const Sudoku = observer(({ sudoku }: { sudoku: SudokuStore }) => (
  <div>
    <SudokuBoardComponent board={sudoku.currentBoard} />
    <ToggleButtonGroup
      type="radio"
      name="board_type"
      value={sudoku.currentBoardType}
      onChange={sudoku.handleChangeBoardType}
    >
      <ToggleButton id="start_board" value={"start_board"}>
        Начальная
      </ToggleButton>
      <ToggleButton
        id="ready_boards"
        value={"ready_boards"}
        disabled={sudoku.readyBoards.length === 0}
      >
        Решённые ({sudoku.readyBoards.length})
      </ToggleButton>
      <br />
      <ToggleButton
        id="progress_boards"
        value={"progress_boards"}
        disabled={sudoku.progressBoards.length === 0}
      >
        В процессе ({sudoku.progressBoards.length})
      </ToggleButton>
      <ToggleButton
        id="failed_boards"
        value={"failed_boards"}
        disabled={sudoku.failedBoards.length === 0}
      >
        Ошибочные ({sudoku.failedBoards.length})
      </ToggleButton>
    </ToggleButtonGroup>
    <div>
      Текущая доска&nbsp;
      <input
        type="number"
        value={sudoku.currentBoardIndex}
        min={0}
        max={sudoku.maxCurrentBoardIndex}
        onChange={sudoku.handleChangeCurrentBoard}
      />
    </div>
    <div style={{ textAlign: "left" }}>
      <input
        type="checkbox"
        title="Автоход"
        checked={sudoku.isAutomove}
        onChange={sudoku.handleToggleAutomove}
      />
      Рекомендованный ход:{" "}
      {sudoku.nextStep === "stop" ? (
        "нет"
      ) : (
        <Button onClick={sudoku.handleNextStep} style={{ width: "150px" }}>
          {nextStepButtonName(sudoku.nextStep)}
        </Button>
      )}
    </div>
    {sudoku.currentBoardType === "start_board" && (
      <div>
        <Button onClick={sudoku.insertData} title="Добавить судоку">
          <FontAwesomeIcon icon={faPaste} />
        </Button>
      </div>
    )}
    {sudoku.currentBoardType === "progress_boards" && (
      <div>
        <Button onClick={sudoku.handleSolveSudoku}>Посчитать</Button>
        <Button onClick={sudoku.copyData} title="Копировать Судоку">
          <FontAwesomeIcon icon={faCopy} />
        </Button>
        <Button onClick={sudoku.handleAddVariants}>Варианты</Button>
        <Button
          onClick={sudoku.handleDeleteCurrentVariant}
          disabled={sudoku.progressBoards.length === 1}
        >
          <FontAwesomeIcon icon={faClose} />
        </Button>
      </div>
    )}
  </div>
));
