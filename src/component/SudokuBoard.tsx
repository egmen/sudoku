import { SudokuBoard } from "../sudoku/sudoku-board.class";
import { SudokuCell } from "../sudoku/sudoku-cell.class";

function RestDigits(props: { cell: SudokuCell }) {
  const { value } = props.cell;

  return (
    <div className="rest-digits">
      {"123456789".split("").map((key) => {
        const hasDigit = value.includes(key);
        return <div key={key}>{hasDigit && key}</div>;
      })}
    </div>
  );
}
export function SudokuBoardComponent({ board }: { board: SudokuBoard }) {
  return (
    <div className="sudoku">
      {board.cells.map((cell, index) => {
        const { value } = cell;
        return (
          <div
            key={index}
            className={
              cell.isError || board.errorCells.includes(cell.index)
                ? "digit-error"
                : ""
            }
          >
            {cell.isSolved || cell.isError ? value : <RestDigits cell={cell} />}
          </div>
        );
      })}
    </div>
  );
}
