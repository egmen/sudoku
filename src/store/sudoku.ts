import React from "react";
import { makeAutoObservable } from "mobx";

import { SudokuBoard } from "../sudoku/sudoku-board.class";

export class SudokuStore {
  autoMoveTimeout = setTimeout(() => null, 0);

  line =
    "57  3 1   4     2 2 68 9  7  4     3            3  4 1 3                4 8 2 7 6";
  // "57  3 1   4     2 2 68 9  7  4     3            3  4 1 3                4 8 2 7 6";
  // "6134587 2728 9  6 59    8 13       695  82  4    14     5       8      74 672  1 ";
  // "9   48  7 38 7  5 6     3 45 4  3      6 184      71 5785    1       4 8  67   9 ";
  // "64 1 73    72 6 51158 9 2  71 6 4 98  57 9         1 22   457  4  9   3 5 6371924"; // С ошибкой
  // "64 1 73    7  6 51158 9 2  7  6 4 9   57 9         1 22   4 7  4  9   3 5 6 71 2 ";
  // "53  7    6  195    98    6 8   6   34  8 3  17   2   6 6    28    419  5    8  79";
  // "2  5  8 3  6 49   51   2 4946    915   1 3   921    8783 4   62   37 5  6 7  8  4";

  startBoard: SudokuBoard;

  readyBoards: SudokuBoard[] = [];

  progressBoards: SudokuBoard[] = [];

  failedBoards: SudokuBoard[] = [];

  currentBoardIndex: number;

  currentBoardType:
    | "start_board"
    | "ready_boards"
    | "progress_boards"
    | "failed_boards";

  /** Автоматический следующих ход */
  isAutomove = false;

  constructor() {
    makeAutoObservable(this);
    this.startBoard = new SudokuBoard(this.line);
    this.progressBoards = [new SudokuBoard(this.line)];
    this.currentBoardIndex = 0;
    this.currentBoardType = "start_board";
  }

  get nextStep() {
    if (this.currentBoard.isError && this.progressBoards.length > 1) {
      return "delete";
    }
    if (this.currentBoard.canMove) {
      return "calc";
    }
    if (this.currentBoard.hasMoves) {
      return "clone";
    }
    if (this.progressBoards[this.currentBoardIndex + 1]) {
      return "next";
    }
    return "stop";
  }

  handleNextStep = () => {
    this.handleChangeBoardType("progress_boards");
    switch (this.nextStep) {
      case "calc":
        this.handleSolveSudoku();
        break;
      case "clone":
        this.handleAddVariants();
        break;
      case "delete":
        this.handleDeleteCurrentVariant();
        break;
      case "next":
        this.readyBoards.push(
          ...this.progressBoards.splice(this.currentBoardIndex, 1)
        );
        break;
      case "stop":
        this.isAutomove = false;
        break;
    }
    if (this.isAutomove) {
      this.autoMoveTimeout = setTimeout(() => this.handleNextStep(), 1);
    }
  };

  get currentBoard() {
    if (this.currentBoardType === "start_board") {
      return this.startBoard;
    }
    if (this.currentBoardType === "progress_boards") {
      return this.progressBoards[this.currentBoardIndex];
    }
    if (this.currentBoardType === "ready_boards") {
      return this.readyBoards[this.currentBoardIndex];
    }
    if (this.currentBoardType === "failed_boards") {
      return this.failedBoards[this.currentBoardIndex];
    }
    return this.startBoard;
  }

  get maxCurrentBoardIndex() {
    if (this.currentBoardType === "progress_boards") {
      return this.progressBoards.length - 1;
    }
    if (this.currentBoardType === "ready_boards") {
      return this.readyBoards.length - 1;
    }
    if (this.currentBoardType === "failed_boards") {
      return this.failedBoards.length - 1;
    }
    return 0;
  }

  set currentBoard(newBoard: SudokuBoard) {
    this.progressBoards = this.progressBoards.map((boardItem, index) =>
      this.currentBoardIndex === index ? newBoard : boardItem
    );
  }

  handleSolveSudoku = () => {
    this.currentBoard = this.currentBoard.getClone().solve();
  };

  handleAddVariants = () => {
    this.progressBoards = this.progressBoards
      .map((boardItem, index) =>
        this.currentBoardIndex === index
          ? this.currentBoard.getVariants()
          : boardItem
      )
      .flat();
  };

  handleChangeCurrentBoard = (event: React.FormEvent<HTMLInputElement>) => {
    this.currentBoardIndex = Number(event.currentTarget.value);
  };

  handleDeleteCurrentVariant = () => {
    this.failedBoards.push(
      ...this.progressBoards.splice(this.currentBoardIndex, 1)
    );
  };

  insertData = async () => {
    const text = await navigator.clipboard.readText();
    if (text.length === 81) {
      this.startBoard = new SudokuBoard(text);
      this.readyBoards = [];
      this.progressBoards = [new SudokuBoard(text)];
      this.failedBoards = [];
      this.currentBoardIndex = 0;
      this.currentBoardType = "start_board";
    }
  };

  copyData = () => {
    navigator.clipboard.writeText(this.currentBoard.export());
  };

  handleToggleAutomove = () => {
    this.isAutomove = !this.isAutomove;
  };

  handleChangeBoardType = (boardType: SudokuStore["currentBoardType"]) => {
    clearInterval(this.autoMoveTimeout);
    if (boardType === "start_board") {
      this.currentBoardIndex = 0;
    }
    if (
      boardType === "ready_boards" &&
      !this.readyBoards[this.currentBoardIndex]
    ) {
      this.currentBoardIndex = this.readyBoards.length - 1;
    }
    if (
      boardType === "progress_boards" &&
      !this.progressBoards[this.currentBoardIndex]
    ) {
      this.currentBoardIndex = this.progressBoards.length - 1;
    }
    if (
      boardType === "failed_boards" &&
      !this.failedBoards[this.currentBoardIndex]
    ) {
      this.currentBoardIndex = this.failedBoards.length - 1;
    }
    this.currentBoardType = boardType;
  };
}

export default new SudokuStore();
