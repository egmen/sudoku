import { SUDOKU_TOKENS, SudokuGroup } from "./sudoku";
import { SudokuCell } from "./sudoku-cell.class";

export class SudokuBoard {
  cells: SudokuCell[] = [];

  constructor(str: string | string[]) {
    if (Array.isArray(str)) {
      this.cells = str.map(
        (cellValue, index) => new SudokuCell(cellValue, index)
      );
    } else {
      const plainString = str.replace(/\n/g, "");
      if (plainString.length !== 81) {
        throw new Error(
          `Некорректный судоку ${plainString.length} символов, должно быть 81`
        );
      }
      this.cells = plainString
        .split("")
        .map(
          (token, index) =>
            new SudokuCell(token === " " ? SUDOKU_TOKENS : token, index)
        );
    }
  }

  get errorCells() {
    return Array.from(
      new Set([
        ...this.checkErrorGroup("row"),
        ...this.checkErrorGroup("col"),
        ...this.checkErrorGroup("cube"),
      ])
    );
  }

  /** Есть ли возможные ходы */
  get hasMoves() {
    return this.cells.some((cell) => cell.isUnsolved);
  }

  /** Содержит ли ошибки */
  get isError() {
    return this.errorCells.length > 0;
  }

  get isSolved() {
    return this.cells.every((cell) => cell.isSolved) && !this.isError;
  }

  checkErrorGroup(group: SudokuGroup) {
    return this.getGroupIds(group)
      .map((groupId) => this.checkErrorGroupId(group, groupId))
      .flat();
  }

  checkErrorGroupId(group: SudokuGroup, groupId: number) {
    const groupCells = this.cells
      .filter((cell) => cell.getGroupId(group) === groupId)
      .filter((cell) => cell.isSolved);

    const allTokens = groupCells.map((cell) => cell.value).join("");
    const repeatedTokens = SudokuBoard.findRepeatTokens(allTokens);

    return groupCells
      .filter((cell) => repeatedTokens.includes(cell.value))
      .map((cell) => cell.index);
  }

  getGroupIds(group: SudokuGroup): number[] {
    return Array.from(
      new Set(this.cells.map((cell) => cell.getGroupId(group)))
    );
  }

  /** Получает копию доски */
  getClone() {
    return new SudokuBoard(this.cells.map((cell) => cell.value));
  }

  solve() {
    this.solveGroup("row");
    this.solveGroup("col");
    this.solveGroup("cube");
    return this;
  }

  solveGroup(group: SudokuGroup) {
    this.getGroupIds(group).forEach((groupId) =>
      this.solveGroupId(group, groupId)
    );
  }

  /**
   * Ищет уникальные токены (символы) в строке
   * @param {string} str
   */
  static findUniqueTokens(str: string) {
    const uniqueTokens = new Set<string>();
    const multiTokens = new Set<string>();
    str.split("").forEach((token) => {
      if (uniqueTokens.has(token)) {
        multiTokens.add(token);
      } else {
        uniqueTokens.add(token);
      }
    });
    return Array.from(uniqueTokens).filter((token) => !multiTokens.has(token));
  }

  /**
   * Ищет повторяющиеся токены (символы) в строке
   * @param {string} str
   */
  static findRepeatTokens(str: string) {
    const uniqueTokens = new Set<string>();
    const multiTokens = new Set<string>();
    str.split("").forEach((token) => {
      if (uniqueTokens.has(token)) {
        multiTokens.add(token);
      } else {
        uniqueTokens.add(token);
      }
    });
    return Array.from(multiTokens);
  }

  /** Решение внутри группы ячеек */
  solveGroupId(group: SudokuGroup, groupId: number) {
    const groupCells = this.cells.filter(
      (cell) => cell.getGroupId(group) === groupId
    );

    /** Удаление решённых значений */
    const solvedCells = groupCells.filter((cell) => cell.isSolved);
    groupCells
      .filter((cell) => cell.isUnsolved)
      .forEach((cell) => {
        solvedCells.forEach((solvedCell) => {
          cell.value = cell.value.replace(solvedCell.value, "");
        });
      });

    /** Обработка уникальных значений */
    const unsolvedTokens = groupCells
      .filter((cell) => cell.isUnsolved)
      .map((cell) => cell.value)
      .join("");
    const uniqueUnsolvedTokens = SudokuBoard.findUniqueTokens(unsolvedTokens);
    groupCells
      .filter((cell) => cell.isUnsolved)
      .forEach((cell) => {
        uniqueUnsolvedTokens.forEach((uniqueToken) => {
          if (cell.value.includes(uniqueToken)) {
            cell.value = uniqueToken;
          }
        });
      });
  }

  /**
   * Получение полного числа вариантов решения судоку
   */
  getVariants(): SudokuBoard[] {
    const [firstSmallUnsolvedCell] = this.cells
      .filter((cell) => cell.isUnsolved)
      .sort((c1, c2) => c1.value.length - c2.value.length);

    return firstSmallUnsolvedCell.value.split("").map((token) => {
      return new SudokuBoard(
        this.cells.map((cell) =>
          firstSmallUnsolvedCell.index === cell.index ? token : cell.value
        )
      );
    });
  }

  /** Можно ли сделать ход */
  get canMove() {
    return !this.isStrictEqual(this.getClone().solve());
  }

  /** Является ли доска на аналогичной стадии решения */
  isStrictEqual(board: SudokuBoard): boolean {
    return board.cells.every(
      (cell) => cell.value === this.cells[cell.index].value
    );
  }

  /** Строка для экспорта */
  export() {
    return this.cells
      .map((cell, index) => {
        const needDigit = cell.value.length === 1 ? `${cell.value}` : " ";
        const isLastInBoard = index === this.cells.length - 1;
        const isLastInRow =
          isLastInBoard || cell.rowId !== this.cells[index + 1].rowId;
        const needNewline = isLastInRow && !isLastInBoard ? "\n" : "";
        return `${needDigit}${needNewline}`;
      })
      .join("");
  }
}
