import { describe, it, expect } from "vitest";

import { SudokuCell } from "./sudoku-cell.class";

describe("SudokuCell class", () => {
  it("calculates row num", () => {
    expect(new SudokuCell("", 0).rowId).toBe(0);
    expect(new SudokuCell("", 8).rowId).toBe(0);
    expect(new SudokuCell("", 9).rowId).toBe(1);
  });

  it("calculates col num", () => {
    expect(new SudokuCell("", 0).colId).toBe(0);
    expect(new SudokuCell("", 8).colId).toBe(8);
    expect(new SudokuCell("", 9).colId).toBe(0);
  });

  it("calculates cube num", () => {
    expect(new SudokuCell("", 0).cubeId).toBe(0);
    expect(new SudokuCell("", 8).cubeId).toBe(2);
    expect(new SudokuCell("", 9).cubeId).toBe(0);
    expect(new SudokuCell("", 27).cubeId).toBe(3);
  });

  it("detects unsolved cell", () => {
    expect(new SudokuCell("12", 0).isUnsolved).toBe(true);
    expect(new SudokuCell("1", 0).isUnsolved).toBe(false);
    expect(new SudokuCell("", 0).isUnsolved).toBe(false);
  });

  it("detects solved cell", () => {
    expect(new SudokuCell("12", 0).isSolved).toBe(false);
    expect(new SudokuCell("1", 0).isSolved).toBe(true);
    expect(new SudokuCell("", 0).isSolved).toBe(false);
  });

  it("detects error cell", () => {
    expect(new SudokuCell("12", 0).isError).toBe(false);
    expect(new SudokuCell("1", 0).isError).toBe(false);
    expect(new SudokuCell("", 0).isError).toBe(true);
  });
});
