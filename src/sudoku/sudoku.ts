export const SUDOKU_WIDTH = 9;

export const CUBE_SIZE = 3;

export const SUDOKU_TOKENS = "123456789";

export type SudokuGroup = "row" | "col" | "cube";
