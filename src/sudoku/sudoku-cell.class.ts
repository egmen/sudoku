import { CUBE_SIZE, SUDOKU_WIDTH, SudokuGroup } from "./sudoku";

export class SudokuCell {
  /** Индекс ячейки */
  index: number;

  /** Значение ячейки */
  value = "";

  constructor(value: string, index: number) {
    this.value = value;
    this.index = index;
  }

  get isSolved() {
    return this.value.length === 1;
  }

  get isUnsolved() {
    return this.value.length > 1;
  }

  get isError() {
    return !this.isSolved && !this.isUnsolved;
  }

  getGroupId(group: SudokuGroup) {
    if (group === "col") {
      return this.colId;
    }
    if (group === "row") {
      return this.rowId;
    }
    if (group === "cube") {
      return this.cubeId;
    }
    throw new Error(`Некорректный тип части судоку: ${group}`);
  }

  get colId() {
    return this.index % SUDOKU_WIDTH;
  }

  get rowId() {
    return Math.floor(this.index / SUDOKU_WIDTH);
  }

  get cubeId() {
    return (
      Math.floor(this.colId / CUBE_SIZE) +
      Math.floor(this.rowId / CUBE_SIZE) * CUBE_SIZE
    );
  }
}
