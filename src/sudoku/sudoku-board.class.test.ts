import { describe, expect, it } from "vitest";
import { SudokuBoard } from "./sudoku-board.class";

describe("sudoku board", () => {
  const TEST_BOARD =
    "57  3 1   4     2 2 68 9  7  4     3            3  4 1 3                4 8 2 7 6";
  const ERROR_BOARD =
    "642157389397286451158493246712624598825719663964453172231845716471962536586371924";

  it("get board", () => {
    const board = new SudokuBoard(TEST_BOARD);

    expect(board.cells[1].value).toBe("7");
    expect(board.cells[2].value).toBe("123456789");
    expect(board.cells[80].value).toBe("6");
  });

  it("get groups id", () => {
    const board = new SudokuBoard(TEST_BOARD);

    expect(board.getGroupIds("col")).toStrictEqual([0, 1, 2, 3, 4, 5, 6, 7, 8]);
    expect(board.getGroupIds("cube")).toStrictEqual([
      0, 1, 2, 3, 4, 5, 6, 7, 8,
    ]);
  });

  it("filteres solved by group id", () => {
    const board = new SudokuBoard(TEST_BOARD);
    expect(board.cells[2].value).toBe("123456789");

    board.solveGroupId("row", 0);

    expect(board.cells[2].value).toBe("24689");
  });

  it("filteres solved by group name", () => {
    const board = new SudokuBoard(TEST_BOARD);
    expect(board.cells[2].value).toBe("123456789");

    board.solveGroup("row");

    expect(board.cells[2].value).toBe("24689");
  });

  it("clears solved rows", () => {
    const board = new SudokuBoard(TEST_BOARD);

    // First row
    expect(board.cells[0].value).toBe("5");
    expect(board.cells[2].value).toContain("5");
    expect(board.cells[8].value).toContain("5");
    // Second row
    expect(board.cells[9].value).toContain("5");

    board.solveGroup("row");

    // First row
    expect(board.cells[0].value).toBe("5");
    expect(board.cells[2].value).not.toContain("5");
    expect(board.cells[8].value).not.toContain("5");
    // Second row
    expect(board.cells[9].value).toContain("5");
  });

  it("find unique token", () => {
    const board = new SudokuBoard(TEST_BOARD);

    board.solve();

    // token 3 is unique for col 1 only in row 5
    expect(board.cells[36].value).toBe("136789");
    board.solveGroupId("col", 0);

    expect(board.cells[36].value).toBe("3");
  });

  it("finds unique tokens in string", () => {
    expect(SudokuBoard.findUniqueTokens("121")).toStrictEqual(["2"]);
    expect(SudokuBoard.findUniqueTokens("1212221")).toStrictEqual([]);
  });

  it("finds repeat tokens in string", () => {
    expect(SudokuBoard.findRepeatTokens("964453172")).toStrictEqual(["4"]);
    expect(SudokuBoard.findRepeatTokens("586371924")).toStrictEqual([]);
  });

  it("list error cells", () => {
    const board = new SudokuBoard(ERROR_BOARD);

    const errorsOfGroup = board.checkErrorGroupId("row", 5);
    expect(errorsOfGroup).toStrictEqual([47, 48]);

    const errorRows = board.checkErrorGroup("row");
    expect(errorRows).toStrictEqual([
      21, 25, 29, 31, 42, 43, 47, 48, 56, 61, 67, 71,
    ]);

    const { errorCells } = board;
    expect(errorCells).toStrictEqual([
      21, 25, 29, 31, 42, 43, 47, 48, 56, 61, 67, 71, 2, 65, 4, 49, 23, 50, 33,
      69, 26, 62, 15, 37, 32,
    ]);
  });

  it("strict equal board", () => {
    const board1 = new SudokuBoard(TEST_BOARD);
    const board2 = new SudokuBoard(TEST_BOARD);

    expect(board1.isStrictEqual(board2)).toBe(true);

    board1.solve();
    expect(board1.isStrictEqual(board2)).toBe(false);
  });

  it("contains errors", () => {
    const successBoard = new SudokuBoard(TEST_BOARD);

    expect(successBoard.isError).toBe(false);

    const errorBoard = new SudokuBoard(ERROR_BOARD);

    expect(errorBoard.isError).toBe(true);
  });
});
